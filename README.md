# 🍓 Raspberry Minecraft Modpack (it's a strawberry, I know)
This _very originally named (totally not after my Raspberry Pi where a server of this modpack is hosted in)_ modpack intends to complement your Minecraft experience playing with the Fabulously Optimized modpack.
It's made with packwiz. You can check their wiki and learn how to setup this on your launcher.

By installing this, I'm assuming you're using an existent Fabulously Optimized instance as a base to it, as Raspberry Modpack doesn't previously include most QoL mods and some dependencies (such as Fabric API).
